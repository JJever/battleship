package battleship;

import battleship.Controller.BoardController;
import battleship.Controller.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("View/sample.fxml"));

        Parent root = loader.load();
        primaryStage.setTitle("Hello!");
        primaryStage.setScene(new Scene(root, 100, 100));
        primaryStage.show();

        MainController controller = loader.getController();
        controller.setStage(primaryStage);
    }


    public static void main(String[] args) {
        launch(args);
    }
}
