package battleship.Model;

import javafx.collections.ObservableList;
import javafx.util.Pair;

import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;


/**
 * Class that controls the process of the game.
 */
public class Game {

    private Socket socket;
    private Board board;
    private boolean waitingForMove; //this variable tells us if Game is waiting for our move
    private int shipFieldsCount;
    private int enemyShipsFieldsCount;
    private boolean reversed = false; //variable used during processof placing ships


    public Game(Socket s) {
        socket = s;
        shipFieldsCount = 0;
        enemyShipsFieldsCount = 0;
    }

    public void reverse() {
        reversed = !reversed;
    }


    /**
     * Function that sets the Board in current game. It must be run before game starts!
     * @param board Board that will be used during the game.
     * @throws NullPointerException If board is null.
     */
    public void setBoard(Board board) {
        if(board == null)
            throw new NullPointerException();
        this.board = board;
    }


    public void move(int x, int y) {
        try {
            boolean outcome = Communicator.attack(x, y, socket);
            if(outcome) { //check if we hit the ship
                enemyShipsFieldsCount--;
                if(enemyShipsFieldsCount == 0) //checks if we won the game
                    finishGame(true);
            }
            else {
                startOpponentTurnThread();
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    //Function that is used to finish the game, clear the boards etc. The parameter tells if we won the game.
    private void finishGame(boolean victory) {
        //TODO
    }

    /*
     * Function returns if the Game if waiting for your move.
     */
    public boolean checkIfWaitingForMove() {
        return waitingForMove;
    }

    /*
     * Function construct thread that is run during opponent's turn.
     */
    private void startOpponentTurnThread() {
        new Thread(() -> {
            waitingForMove = false;
            while (!waitingForMove) {
                try {
                    Pair<Integer, Integer> p = Communicator.waitForResponse(socket); //get message from opponent
                    ShipState answer = board.clickMyPiece(p.getKey(), p.getValue()); //click piece and check if that was a ship
                    Communicator.sendAnswer(answer, socket); //send message back
                    if(answer == ShipState.MISS) //if opponent missed, we start our turn
                        waitingForMove = true;
                    else { //otherwise, we have to check if we lost, //TODO rest
                        shipFieldsCount--;
                        if(shipFieldsCount == 0)
                            finishGame(false);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }
            }
        }).start();
    }


    /**
     * Function that is used to start the game.
     * @param startingPlayer Tells if we are the starting player.
     */
    public void prepareGame(boolean startingPlayer) {
        List<Pair<Integer, Integer>> list = new ArrayList<>();
        Board.PlacingPane[][] panes = board.constructPlacingShipsBoard(list);
        new PlacingShips(5, 1, panes, startingPlayer, this);
    }



    void startGame(boolean startingPlayer, List<Ship> list) {
        board.constructMainBoard(list);
        waitingForMove = startingPlayer;
        if(!waitingForMove)
            startOpponentTurnThread();
    }

}

