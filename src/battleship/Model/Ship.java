package battleship.Model;


import javafx.util.Pair;

import java.util.List;

/**
 * Class taking care of ships.
 */
public class Ship {
    private int remainingFields;
    private List<Pair<Integer, Integer>> list; //list of coordinates of fields, where ship is placed

    public Ship(int numberOfFields, List<Pair<Integer, Integer>> l) {
        list = l;
        remainingFields = numberOfFields;
    }

    /**
     * Function that reduces remainingFields number by 1.
     * @return True if the ship is destroyed, false if not.
     */
    public boolean hit() {
        remainingFields--;
        return remainingFields == 0;
    }

    public List<Pair<Integer, Integer>> getList() {
        return list;
    }
}
