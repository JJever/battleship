package battleship.Model;

public enum ShipState {
    MISS, HIT, DESTROYED
}
