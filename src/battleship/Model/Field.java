package battleship.Model;

import javafx.scene.layout.Pane;
import javafx.util.Pair;

import java.awt.event.MouseEvent;
import java.beans.EventHandler;
import java.io.IOException;
import java.net.Socket;


/**
 * Class that controls behaviour of small squares on the board.
 */
public class Field extends Pane {
    private Ship ship; //keeps reference to the Ship, that is on that field. If there is none, it's null.
    private boolean wasTargeted; //tells if the field was already targeted.
    private boolean enemy; //tells if the field lays on your opponent board.
    private int posx; //x coordinate of the field on grid.
    private int posy; //y coordinate of the field on grid.
    private Game game; //reference to Game class, that controls actual game

    /**
     *
     * @param enemy Tells if the field will be used on my, or opponent board.
     */
    public Field(boolean enemy, int posx, int posy, Game game) {
        ship = null;
        wasTargeted = false;
        this.enemy = enemy;
        this.setStyle("-fx-background-color: blue");
        this.posx = posx;
        this.posy = posy;
        this.game = game;

        this.setOnMouseClicked(MouseEvent -> {
            if (this.enemy && !wasTargeted && this.game.checkIfWaitingForMove()) {
                wasTargeted = true;
                this.setStyle("-fx-background-color: black");
                this.game.move(this.posx, this.posy);
            }
        });
    }

    /**
     * Function marks the field as clicked.
     * @return True if there was a ship on the field.
     * @throws IllegalStateException If the field was already targeted, or it's an enemy's field.
     */
    public ShipState click() throws IllegalStateException {
        if(wasTargeted || enemy)
            throw new IllegalStateException();
        wasTargeted = true;
        this.setStyle("-fx-background-color: black");
        if(ship == null)
            return ShipState.MISS;
        if(ship.hit())
            return ShipState.DESTROYED;
        return ShipState.HIT;
    }


    public void setShip(Ship ship) {
        this.ship = ship;
    }
}


