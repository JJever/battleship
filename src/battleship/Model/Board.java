package battleship.Model;

import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.awt.event.MouseEvent;
import java.net.Socket;
import java.util.List;

public class Board {

    private GridPane enemyBoard;
    private GridPane myBoard;
    private Field[][] myPieces;
    private VBox parent;
    private Game game;



    public Board(VBox parent, Game game) {
        this.parent = parent;
        this.game = game;
    }


    /*
     * Function adds columns to the grid.
     */
    private void setColumns(GridPane grid) {
        for (int i = 0; i < 8; i++) {
            ColumnConstraints colConst = new ColumnConstraints();
            colConst.setPercentWidth(100.0 / 8);
            grid.getColumnConstraints().add(colConst);
        }
    }

    /*
     * Function adds rows to the grid.
     */
    private void setRows(GridPane grid) {
        for (int i = 0; i < 8; i++) {
            RowConstraints rowConst = new RowConstraints();
            rowConst.setPercentHeight(100.0 / 8);
            rowConst.setMinHeight(20);
            grid.getRowConstraints().add(rowConst);
        }
    }


    /*
     * Function fills Grid with new Fields. Parameter enemy tells if the Grid belongs to your opponent.
     */
    private void fillBoard(GridPane grid, boolean enemy, Game game, List<Ship> list) {
        for(int i = 0; i < 8; i++) {
            for(int j = 0; j < 8; j++) {
                Field field = new Field(enemy, i, j, game);
                if(!enemy)
                    myPieces[i][j] = field;
                field.setMaxSize(100, 100);
                field.setMinSize(10, 10);
                grid.add(field, i, j);
            }
        }
        if(list != null) {
            for (Ship s : list) {
                for (Pair<Integer, Integer> p : s.getList()) {
                    myPieces[p.getKey()][p.getValue()].setShip(s);
                }
            }
        }
    }


    /*
     * Function that creates the Board.
     */
    private void setBoard(GridPane grid, VBox parent, boolean enemy, Game game, List<Ship> list) {
        setColumns(grid);
        setRows(grid);

        fillBoard(grid, enemy, game, list);

        grid.setVgap(5);
        grid.setHgap(5);

        parent.getChildren().add(grid);
    }

    /**
     * Function that marks the Field, on position given by arguments, targeted.
     * @param x coordinate of the Field.
     * @param y coordinate of the Field.
     * @return True if there was a ship on the Field, false otherwise.
     * @throws IndexOutOfBoundsException If x < 0 or x > 8 or y < 0 or y > 8.
     * @throws IllegalStateException If the field was already targeted.
     */
    public ShipState clickMyPiece(int x, int y)
            throws IndexOutOfBoundsException, IllegalStateException{
        if(x < 0 || x > 8 || y < 0 || y > 8) {
            String errorMessage = "x = " + Integer.toString(x) + "y = " + Integer.toString(y);
            throw new IndexOutOfBoundsException(errorMessage);
        }
        return myPieces[x][y].click();
    }

    public void constructMainBoard(List<Ship> list) {
        enemyBoard = new GridPane();
        myBoard = new GridPane();
        myPieces = new Field[8][8];

        //Constructing the Window.
        parent.getChildren().add(new Label("Enemy"));
        setBoard(enemyBoard, parent, true, game, null);
        parent.getChildren().add(new Label("You"));
        setBoard(myBoard, parent, false, game, list); //TODO dodać list do listy argumentów
    }

    /*
     * Class that is used during ships placing, used as Panes in GridPane
     */
    public static class PlacingPane extends Pane {
        private List<Pair<Integer, Integer>> list;
        private int x;
        private int y;
        private boolean choosen = false;
        PlacingShips ps;

        public PlacingPane(int x, int y, List<Pair<Integer, Integer>> list) {
            if(list == null)
                throw new NullPointerException();
            this.x = x;
            this.y = y;
            this.list = list;
            this.setStyle("-fx-background-color: blue");
        }

        public List<Pair<Integer, Integer>> getList() {
            return list;
        }

        public void setMouseEvent() {
            this.setOnMouseEntered(MouseEvent -> {
                if(!choosen) {
                    //this.setStyle("-fx-background-color: orangered");
                    ps.markPanes(true, false, x, y);
                }
            });

            this.setOnMouseExited(MouseEvent -> {
                if(!choosen) {
                    //this.setStyle("-fx-background-color: black");
                    ps.markPanes(false, false, x, y);
                }
            });

            this.setOnMouseClicked(MouseEvent -> {
                if(!choosen) {
                    if(ps.markPanes(true, true, x, y))
                        ps.placed();
                }
            });
        }

        public void setPlacingShip(PlacingShips ps) {
            this.ps = ps;
        }

        public void setChoosen(boolean b) {
            choosen = b;
        }

        public boolean wasChoosen() {
            return choosen;
        }
    }

    public PlacingPane[][] constructPlacingShipsBoard(List<Pair<Integer, Integer>> list) {
        GridPane grid = new GridPane();

        setColumns(grid);
        setRows(grid);


        PlacingPane[][] panes = new PlacingPane[8][8];


        for(int i = 0; i < 8; i++) {
            for(int j = 0; j < 8; j++) {
                PlacingPane pane = new PlacingPane(i, j, list);
                panes[i][j] = pane;
                grid.add(pane, i, j);
            }
        }
        Stage stage = new Stage();
        stage.setScene(new Scene(grid, 500, 500));
        stage.show();

        return panes;
    }
}
