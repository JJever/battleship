package battleship.Model;

import javafx.util.Pair;

import java.io.IOException;
import java.net.Socket;


/**
 * Class used to communicate between you and your opponent.
 */
public class Communicator {


    /*
     * Function encodes the message.
     */
    private static int encode(int x, int y) {
        return 10 * x + y;
    }

    /*
     * Function decodes message
     */
    private static Pair<Integer, Integer> decode(int m) {
        int x = m / 10;
        int y = m - x * 10;
        return new Pair<>(x, y);
    }


    /**
     * Funtion that sends information about choosen field, and returns the result of the attack.
     * @param x Coordinate of the field.
     * @param y Coordinate of the field.
     * @return 1 if the attack was succesful, 0 if not
     * @throws IndexOutOfBoundsException If x < 0 or x < 8 or y < 0 or y > 8
     * @throws IOException When something's wrong with the socket, or the message received is illegal.
     */
    public static boolean attack(int x, int y, Socket socket)
            throws IndexOutOfBoundsException, IOException {
        if(x < 0 || x > 8 || y < 0 || y > 8) {
            String errorMessage = "x = " + Integer.toString(x) + "y = " + Integer.toString(y);
            throw new IndexOutOfBoundsException(errorMessage);
        }
        int message = encode(x, y);
        socket.getOutputStream().write(message);
        int answer = socket.getInputStream().read();
        if(answer != 0 && answer != 1)
            throw new IOException("The message received is wrong. The opponent may be cheating!");
        return answer != 0;
    }

    /**
     * Function that waits for response from other player, and returns coordinates of clicked field.
     * @param socket Socket, that the game is connected to.
     * @return Coordinates of clicked field.
     */
    public static Pair<Integer, Integer> waitForResponse(Socket socket) throws IOException {
        int message = socket.getInputStream().read();
        return decode(message);
    }

    /**
     * Function that sends back to the opponent, if there was a ship on the field he choose.
     * @param hit Parameter that tells, if there was a ship on the field, and if it was destroyed.
     * @param socket Socket, that the Game is connected to.
     */
    public static void sendAnswer(ShipState hit, Socket socket) throws IOException {
        if(hit.equals(ShipState.DESTROYED))
            socket.getOutputStream().write(2);
        if(hit.equals(ShipState.HIT))
            socket.getOutputStream().write(1);
        if(hit.equals(ShipState.MISS))
            socket.getOutputStream().write(0);
    }

}
