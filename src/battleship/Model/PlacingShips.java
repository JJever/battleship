package battleship.Model;


import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that is responsible for process of placing ships on the board.
 */
public class PlacingShips {

    private int initSize;
    private int initAmount;
    Board.PlacingPane[][] panes;
    boolean startingPlayer;
    Game game;
    private int size;
    private int amount;
    private boolean reversed; //TODO add reverse button
    List<Ship> ships; //list that holds coordinates of fields, where ships are


    public PlacingShips(int size, int amount, Board.PlacingPane[][] panes, boolean starting, Game game)
        throws IllegalArgumentException {
        if(size < 1 || size > 5)
            throw new IllegalArgumentException("Size must be between 1 and 5. size = " + Integer.toString(size));

        this.initSize = size;
        this.initAmount = amount;
        this.panes = panes;
        this.startingPlayer = starting;
        this.game = game;
        this.size = initSize;
        this.amount = initAmount;
        this.reversed = false;
        this.ships = new ArrayList<>();
        placeShip();
    }


    private void placeShip() {
        for(int i = 0; i < 8; i++)
            for(int j = 0; j < 8; j++) {
                panes[i][j].setPlacingShip(this);
                panes[i][j].setMouseEvent();
            }
    }


    /**
     * Fuction that places ship of previously given size, or marks the fields to show where ship will be, on point (x, y).
     * @param mark Tells if we want to mark the field. If false, we just change color to deafult (blue)
     * @param choose Tells if we want to choose the field to be a ship.
     * @param x Coordinate of field, that we want to place ship on.
     * @param y Coordinate of field, that we want to place ship on.
     * @throws IllegalStateException
     */
    public boolean markPanes(boolean mark, boolean choose, int x, int y)
            throws IllegalStateException {

        List<Pair<Integer, Integer>> l = check(x, y);
        if(l == null)
            return false;


        String color;

        if(choose) {
            color = "red";
        }
        else {
            if(mark) {
                color = "orangered";
            }
            else {
                color = "blue";
            }
        }

        markCells(choose, l, color);

        return true;

    }


    /**
     * Function that tries to build a ship up/to left.
     * @param j Number of fields we want to go up/left.
     * @param xx Pos of the ship.
     * @param yy Pos of the ship.
     * @param l List that we save potential coordinates that we want to place ship on.
     * @return -1 if we can't put ship there. Otherwise, function returns j.
     */
    private int goUp(int j, int xx, int yy, List<Pair<Integer, Integer>> l) {
        while(j > 0 && xx >= 0 && yy >= 0) {
            if(!panes[xx][yy].wasChoosen()) {
                l.add(new Pair<>(xx, yy));
                j--;
                if(reversed) xx--;
                else yy--;
            }
            else return -1;
        }
        return j;
    }


    //Function like goUp
    private int goDown(int j, int xx, int yy, List<Pair<Integer, Integer>> l) {
        if(reversed) xx++;
        else yy++;
        while(j > 0 && xx < 8 && yy < 8) {
            if(!panes[xx][yy].wasChoosen()) {
                l.add(new Pair<>(xx, yy));
                j--;
                if(reversed) xx++;
                else yy++;
            }
            else return -1;
        }
        return j;
    }

    //Function checks if we can place ship on the field (x, y). Returns list if possible, otherwise returns null.
    private List<Pair<Integer, Integer>> check(int x, int y) {
        int j = size / 2; //number of fields above/on the left of (x, y)
        int i = size - j; //number of fields under/on the right of (x, y)
        List<Pair<Integer, Integer>> l = new ArrayList<>(); //list, that holds fields, to place ship on


        int jj = goUp(j, x, y, l);
        if(jj == -1)
            return null;
        i += jj;
        int ii = goDown(i, x, y, l);
        if(ii == -1)
            return null;

        if(ii > 0) {
            if(reversed) x -= j;
            else y -= j;
            if(goUp(ii, x, y, l) == -1)
                return null;
        }

        return l;

    }


    /**
     * Function that marks cells in l on the given color.
     * @param choose If we want to choose the field (place ship on it).
     * @param l List of coordinates.
     * @param color Color that we will mark the cells in list.
     */
    private void markCells(boolean choose, List<Pair<Integer, Integer>> l, String color) {
        for (Pair<Integer, Integer> pair : l) {
            int xx = pair.getKey();
            int yy = pair.getValue();
            panes[xx][yy].setStyle("-fx-background-color: " + color);
            panes[xx][yy].setChoosen(choose);
        }
        if(choose) {
            Ship ship = new Ship(size, l);
            ships.add(ship);
        }
    }


    /**
     * Function that goes to the next step: decreases number of ships needed and ,if necessary, lowers next ship size.
     */
    public void placed() {
        if(--amount == 0) {
            if(--size == 0) {
                game.startGame(startingPlayer, ships);
            }
            amount = 6 - size;
        }
    }


}
