package battleship.Controller;


import javafx.application.Platform;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.*;

import static battleship.Controller.BoardController.openBoard;

public class HostController {

    private Stage primaryStage;

    public HostController() throws IOException {
        Thread t = new Thread(
                () -> {
                    try {
                        ServerSocket serverSocket = new ServerSocket(5555);
                        Socket socket = serverSocket.accept();
                        Platform.runLater(() -> {
                            try {
                                openBoard(socket, primaryStage, getClass(), true);
                            } catch(IOException e) {
                                e.printStackTrace();
                            }
                        });
                    } catch(IOException e) {
                        e.printStackTrace();
                    }
                }
        );
        t.setDaemon(true);
        t.start();
    }


    void setStage(Stage s) {
        primaryStage = s;
    }

}
