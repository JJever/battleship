package battleship.Controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import static battleship.Controller.BoardController.openBoard;

public class ClientController {

    private Stage primaryStage;


    public void setStage(Stage s) {
        primaryStage = s;
    }

    public void openWindow() throws IOException{
        Socket socket = new Socket("127.0.1.1", 5555);
        openBoard(socket, primaryStage, getClass(), false);
    }
}
