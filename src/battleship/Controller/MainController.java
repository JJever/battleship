package battleship.Controller;

import battleship.Main;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.awt.event.MouseEvent;
import java.beans.EventHandler;
import java.io.IOException;

public class MainController {
    @FXML
    Button host;

    @FXML
    Button join;

    private Stage primaryStage;

    @FXML
    public void initialize() {
        host.setOnMouseClicked(MouseEvent -> {
            try {
                openWindow(true);
            } catch(IOException e) {
                e.printStackTrace();
            }
        });

        join.setOnMouseClicked(MouseEvent -> {
            try {
                openWindow(false);
            } catch(IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void setStage(Stage s) {
        primaryStage = s;
    }

    private void openWindow(boolean isHost) throws IOException{
        String fxmlFile;
        if(isHost)
            fxmlFile = "../View/host.fxml";
        else
            fxmlFile = "../View/client.fxml";

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(fxmlFile));

        Parent root = loader.load();
        primaryStage.setTitle("Window");
        primaryStage.setScene(new Scene(root, 200, 200));
        primaryStage.show();

        if(isHost) {
            HostController controller = loader.getController();
            controller.setStage(primaryStage);
        }
        else {
            ClientController controller = loader.getController();
            controller.setStage(primaryStage);
            controller.openWindow();
        }
    }
}
