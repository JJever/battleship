package battleship.Controller;

import battleship.Main;
import battleship.Model.Board;
import battleship.Model.Game;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;


public class BoardController {

    @FXML
    public VBox parent;

    private Socket socket;

    @FXML
    public void initialize() {
        parent.setSpacing(10);
    }


    public void setSocket(Socket s) {
        socket = s;
    }


    public static void openBoard(Socket s, Stage primaryStage, Class c, boolean host) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(c.getResource("../View/board.fxml"));
        Parent root = loader.load();
        BoardController controller = loader.getController();
        controller.setSocket(s);
        primaryStage.setScene(new Scene(root, 500, 500));
        controller.startGame(host);
    }

    private void startGame(boolean host) {
        Game g = new Game(socket);
        Board b = new Board(parent, g);
        g.setBoard(b);
        g.prepareGame(host);
    }

}
